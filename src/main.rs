#![feature(slice_patterns)]

use std::iter;

fn _last(coll: &[i32]) -> Option<i32> {
    match coll {
        [] => None,
        [x] => Some(*x),
        [_, xs..] => _last(xs),
    }
}

#[test]
fn test_last() {
    assert_eq!(_last(&[]), None);
    assert_eq!(_last(&[100]), Some(100));
    assert_eq!(_last(&[1, 1, 2, 3, 5, 8]), Some(8));
}

fn _penultimate(coll: &[i32]) -> Option<i32> {
    match coll {
        [] | [_] => None,
        [x, _] => Some(*x),
        [_, xs..] => _penultimate(xs),
    }
}

#[test]
fn test_penultimate() {
    assert_eq!(_penultimate(&[]), None);
    assert_eq!(_penultimate(&[100]), None);
    assert_eq!(_penultimate(&[50, 100]), Some(50));
    assert_eq!(_penultimate(&[1, 1, 2, 3, 5, 8]), Some(5));
}

fn _nth(idx: usize, coll: &[i32]) -> Option<&i32> {
    coll.into_iter().skip(idx).next()
}

#[test]
fn test_nth() {
    assert_eq!(_nth(0, &[]), None);
    assert_eq!(_nth(1, &[]), None);
    assert_eq!(_nth(1, &[1]), None);
    assert_eq!(_nth(2, &[1, 1, 2, 3, 5, 8]), Some(&2));
}

fn _length<T>(coll: &[T]) -> u32 {
    coll.iter().map(|_| 1).sum()
}

#[test]
fn test_length() {
    let mut param: Vec<i32> = Vec::new();
    assert_eq!(_length(&param), 0);
    param = vec![1];
    assert_eq!(_length(&param), 1);
    param = vec![1, 1, 2, 3, 5, 8];
    assert_eq!(_length(&param), 6);
}

fn _reverse(coll: &[i32]) -> Vec<i32> {
    match coll {
        [] => vec![],
        [xs.., x] => [*x]
            .iter()
            .chain(_reverse(xs).iter())
            .cloned()
            .collect::<Vec<_>>(),
    }
}

#[test]
fn test_reverse() {
    let param: Vec<i32> = Vec::new();
    let expected: Vec<i32> = Vec::new();
    assert_eq!(_reverse(&param), expected);

    let param: Vec<i32> = vec![100];
    let expected: Vec<i32> = vec![100];
    assert_eq!(_reverse(&param), expected);

    let param: Vec<i32> = vec![1, 1, 2, 3, 5, 8];
    let expected: Vec<i32> = vec![8, 5, 3, 2, 1, 1];
    assert_eq!(_reverse(&param), expected);
}

fn _is_palindrome(coll: &[i32]) -> bool {
    match coll {
        [] | [_] => true,
        [x, xs.., y] => x == y && _is_palindrome(xs),
    }
}

#[test]
fn test_is_palindrome() {
    assert!(_is_palindrome(&[]));
    assert!(_is_palindrome(&[1]));
    assert!(_is_palindrome(&[1, 1]));
    assert!(_is_palindrome(&[1, 2, 1]));
    assert!(_is_palindrome(&[1, 2, 3, 2, 1]));
    assert!(!_is_palindrome(&[1, 2]));
    assert!(!_is_palindrome(&[1, 2, 3]));
    assert!(!_is_palindrome(&[1, 2, 3, 2, 3]));
}

// TODO
fn _flatten() {}

fn _compress(coll: &[char]) -> Vec<char> {
    match coll {
        [] => vec![],
        _ => (coll[..1])
            .iter()
            .chain(
                (coll[1..])
                    .iter()
                    .zip(coll)
                    .filter(|(x, y)| x != y)
                    .map(|(x, _)| x),
            )
            .cloned()
            .collect::<Vec<_>>(),
    }
}

#[test]
fn test_compress() {
    let result: Vec<char> = vec![];
    assert_eq!(_compress(&[]), result);

    let result: Vec<char> = vec!['a', 'b', 'c'];
    assert_eq!(_compress(&['a', 'b', 'c']), result);

    let result: Vec<char> = vec!['a'];
    assert_eq!(_compress(&['a', 'a']), result);

    let result: Vec<char> = vec!['a', 'b', 'c', 'a', 'd', 'e'];
    assert_eq!(
        _compress(&['a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e']),
        result
    );
}

fn _pack(coll: &[char]) -> Vec<Vec<char>> {
    let mut packed: Vec<Vec<char>> = Vec::new();
    match coll {
        [] => packed,
        [x, ys..] => {
            let mut grouped: Vec<char> = vec![*x];
            for y in ys {
                match &grouped[..] {
                    [] => grouped.push(*y),
                    [.., z] => {
                        if y == z {
                            grouped.push(*y);
                        } else {
                            packed.push(grouped);
                            grouped = vec![*y];
                        }
                    }
                }
            }
            packed.push(grouped);
            packed
        }
    }
}

#[test]
fn test_pack() {
    let mut expected: Vec<Vec<char>> = Vec::new();
    assert_eq!(_pack(&[]), expected);
    expected = vec![vec!['a']];
    assert_eq!(_pack(&['a']), expected);
    expected = vec![
        vec!['a', 'a', 'a', 'a'],
        vec!['b'],
        vec!['c', 'c'],
        vec!['a', 'a'],
        vec!['d'],
        vec!['e', 'e', 'e', 'e'],
    ];
    assert_eq!(
        _pack(&['a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e']),
        expected
    );
}

fn _encode(coll: &[char]) -> Vec<(u32, char)> {
    match coll {
        [] => vec![],
        [x, ys..] => {
            let mut encoded: Vec<(u32, char)> = Vec::new();
            let mut current: char = *x;
            let mut count: u32 = 1;
            for y in ys {
                if *y == current {
                    count += 1
                } else {
                    encoded.push((count, current));
                    current = *y;
                    count = 1;
                }
            }
            encoded.push((count, current));
            encoded
        }
    }
}

#[test]
fn test_encode() {
    assert_eq!(_encode(&[]), vec![]);
    assert_eq!(_encode(&['a']), vec![(1, 'a')]);
    assert_eq!(
        _encode(&['a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e']),
        vec![(4, 'a'), (1, 'b'), (2, 'c'), (2, 'a'), (1, 'd'), (4, 'e')]
    );
}

// TODO
fn _encode_modified() {}

fn _decode(coll: Vec<(usize, char)>) -> Vec<char> {
    coll.into_iter()
        .flat_map(|(n, c)| iter::repeat(c).take(n))
        .collect::<Vec<_>>()
}

#[test]
fn test_decode() {
    assert_eq!(_decode(vec![]), vec![]);
    assert_eq!(_decode(vec![(3, 'z')]), vec!['z', 'z', 'z']);
    assert_eq!(
        _decode(vec![
            (4, 'a'),
            (1, 'b'),
            (2, 'c'),
            (2, 'a'),
            (1, 'd'),
            (4, 'e')
        ]),
        vec!['a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e']
    )
}

fn _duplicate<T>(coll: &[T]) -> Vec<&T> {
    coll.iter()
        .flat_map(|x| iter::repeat(x).take(2))
        .collect::<Vec<_>>()
}

#[test]
fn test_duplicate() {
    let param: &[i32] = &[];
    let expected: Vec<&i32> = Vec::new();
    assert_eq!(_duplicate(param), expected);
    assert_eq!(_duplicate(&['a']), vec![&'a', &'a']);
    assert_eq!(_duplicate(&['z', 'z']), vec![&'z', &'z', &'z', &'z']);
    assert_eq!(
        _duplicate(&['a', 'b', 'c', 'd', 'e']),
        vec![&'a', &'a', &'b', &'b', &'c', &'c', &'d', &'d', &'e', &'e']
    );
}

fn _duplicate_n<T>(n: usize, coll: &[T]) -> Vec<&T> {
    coll.iter()
        .flat_map(|x| iter::repeat(x).take(n))
        .collect::<Vec<_>>()
}

#[test]
fn test_duplicate_n() {
    let mut param: &[char] = &['a', 'b', 'c'];
    let mut expected: Vec<&char> = Vec::new();
    assert_eq!(_duplicate_n(0, param), expected);
    param = &[];
    expected = Vec::new();
    assert_eq!(_duplicate_n(100, param), expected);
    assert_eq!(
        _duplicate_n(3, &['a', 'b', 'c', 'd', 'e']),
        vec![
            &'a', &'a', &'a', &'b', &'b', &'b', &'c', &'c', &'c', &'d', &'d', &'d', &'e', &'e',
            &'e'
        ]
    );
}

fn _drop<T>(n: usize, coll: &[T]) -> Vec<&T> {
    coll.into_iter()
        .enumerate()
        .filter(|(idx, _)| (idx + 1) % n != 0)
        .map(|(_, x)| x)
        .collect::<Vec<_>>()
}

#[test]
fn test_drop() {
    let param: &[i32] = &[];
    let expected: Vec<&i32> = Vec::new();
    assert_eq!(_drop(3, param), expected);

    let param: &[char] = &['a', 'b', 'c', 'd'];
    let expected: Vec<&char> = vec![&'a', &'c'];
    assert_eq!(_drop(2, param), expected);

    let param: &[char] = &['a', 'b', 'c', 'd'];
    let expected: Vec<&char> = Vec::new();
    assert_eq!(_drop(1, param), expected);

    let param: &[char] = &['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k'];
    let expected: Vec<&char> = vec![&'a', &'b', &'d', &'e', &'g', &'h', &'j', &'k'];
    assert_eq!(_drop(3, param), expected);
}

fn _split<T>(n: usize, coll: &[T]) -> [Vec<&T>; 2] {
    let left = coll.iter().take(n);
    let right = coll.iter().skip(n);
    [left.collect::<Vec<_>>(), right.collect::<Vec<_>>()]
}

#[test]
fn test_split() {
    let param: &[i32] = &[];
    let expected: [Vec<&i32>; 2] = [vec![], vec![]];
    assert_eq!(_split(0, param), expected);

    let param: &[char] = &['a', 'b'];
    let expected: [Vec<&char>; 2] = [vec![&'a', &'b'], vec![]];
    assert_eq!(_split(2, param), expected);

    let param: &[char] = &['a', 'b'];
    let expected: [Vec<&char>; 2] = [vec![&'a', &'b'], vec![]];
    assert_eq!(_split(100, param), expected);

    let param: &[char] = &['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k'];
    let expected: [Vec<&char>; 2] = [
        vec![&'a', &'b', &'c'],
        vec![&'d', &'e', &'f', &'g', &'h', &'i', &'j', &'k'],
    ];
    assert_eq!(_split(3, param), expected)
}

fn _slice<T>(from: usize, to: usize, coll: &[T]) -> Vec<&T> {
    coll.iter().skip(from).take(to - from).collect::<Vec<_>>()
}

#[test]
fn test_slice() {
    let param: &[i32] = &[];
    let expected: Vec<&i32> = Vec::new();
    assert_eq!(_slice(100, 200, param), expected);

    let param: &[char] = &['a', 'b', 'c', 'd', 'e'];
    let expected: Vec<&char> = vec![&'b', &'c'];
    assert_eq!(_slice(1, 3, param), expected);

    let param: &[char] = &['a', 'b', 'c', 'd', 'e'];
    let expected: Vec<&char> = vec![&'b', &'c', &'d', &'e'];
    assert_eq!(_slice(1, 100, param), expected);

    let param: &[char] = &['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k'];
    let expected: Vec<&char> = vec![&'d', &'e', &'f', &'g'];
    assert_eq!(_slice(3, 7, param), expected);
}

fn _rotate(n: isize, coll: &[char]) -> Vec<char> {
    let len = coll.len();
    let shift_to_right = if n > 0 {
        n as usize
    } else {
        (len as isize + n) as usize
    };
    coll.iter()
        .cycle()
        .skip(shift_to_right)
        .take(len)
        .cloned()
        .collect()
}

#[test]
fn test_rotate() {
    assert_eq!(_rotate(0, &['a', 'b', 'c']), vec!['a', 'b', 'c']);
    assert_eq!(_rotate(100, &[]), vec![]);
    assert_eq!(_rotate(3, &['a', 'b', 'c', 'd']), vec!['d', 'a', 'b', 'c']);
}

fn _remove_at(n: usize, coll: &[char]) -> (Vec<char>, Option<char>) {
    (
        coll.iter()
            .take(n)
            .chain(coll.iter().skip(n + 1))
            .cloned()
            .collect::<Vec<char>>(),
        match coll.iter().nth(n) {
            Some(c) => Some(*c),
            None => None,
        },
    )
}

#[test]
fn test_remove_at() {
    assert_eq!(
        _remove_at(1, &['a', 'b', 'c', 'd']),
        (vec!['a', 'c', 'd'], Some('b'))
    )
}

fn main() {}
